<?php

namespace App\Repository;

use App\Entity\Timeoffset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Timeoffset|null find($id, $lockMode = null, $lockVersion = null)
 * @method Timeoffset|null findOneBy(array $criteria, array $orderBy = null)
 * @method Timeoffset[]    findAll()
 * @method Timeoffset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeoffsetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Timeoffset::class);
    }

    // /**
    //  * @return Timeoffset[] Returns an array of Timeoffset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Timeoffset
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
