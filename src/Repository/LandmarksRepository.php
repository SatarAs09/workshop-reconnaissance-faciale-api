<?php

namespace App\Repository;

use App\Entity\Landmarks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Landmarks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Landmarks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Landmarks[]    findAll()
 * @method Landmarks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LandmarksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Landmarks::class);
    }

    // /**
    //  * @return Landmarks[] Returns an array of Landmarks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Landmarks
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
