<?php

namespace App\Entity;

use App\Repository\TimestampRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimestampRepository::class)
 */
class Timestamp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToMany(targetEntity=Attribute::class, mappedBy="timestamp")
     */
    private ?Collection $attribute;

    /**
     * @ORM\OneToMany(targetEntity=Landmarks::class, mappedBy="timestamp")
     */
    private ?Collection $landmarks;

    /**
     * @ORM\OneToOne(targetEntity=NormalisedBoundingBox::class, cascade={"persist", "remove"})
     */
    private ?Collection $normalisedBoundingBox;

    /**
     * @ORM\OneToMany(targetEntity=Timeoffset::class, mappedBy="timestamp")
     */
    private ?Collection $timeoffset;

    public function __construct()
    {
        $this->attribute = new ArrayCollection();
        $this->landmarks = new ArrayCollection();
        $this->timeoffset = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttributes(): Collection
    {
        return $this->attribute;
    }

    public function addAttribute(Attribute $attribute): self
    {
        if (!$this->attribute->contains($attribute)) {
            $this->attribute[] = $attribute;
            $attribute->setTimestamp($this);
        }

        return $this;
    }

    public function removeAttribute(?Attribute $attribute): self
    {
        if ($this->attribute->removeElement($attribute)) {
            // set the owning side to null (unless already changed)
            if ($attribute->getTimestamp() === $this) {
                $attribute->setTimestamp(null);
            }
        }

        return $this;
    }

    public function getLandmarks(): Collection
    {
        return $this->landmarks;
    }

    public function addLandmark(Landmarks $landmark): self
    {
        if (!$this->landmarks->contains($landmark)) {
            $this->landmarks[] = $landmark;
            $landmark->setTimestamp($this);
        }

        return $this;
    }

    public function removeLandmark(Landmarks $landmark): self
    {
        if ($this->landmarks->removeElement($landmark)) {
            // set the owning side to null (unless already changed)
            if ($landmark->getTimestamp() === $this) {
                $landmark->setTimestamp(null);
            }
        }

        return $this;
    }

    public function getNormalisedBoundingBox(): Collection
    {
        return $this->normalisedBoundingBox;
    }

    public function setNormalisedBoundingBox(?NormalisedBoundingBox $normalisedBoundingBox): self
    {
        $this->normalisedBoundingBox = $normalisedBoundingBox;

        return $this;
    }

    public function getTimeoffset(): Collection
    {
        return $this->timeoffset;
    }

    public function addTimeoffset(Timeoffset $timeoffset): self
    {
        if (!$this->timeoffset->contains($timeoffset)) {
            $this->timeoffset[] = $timeoffset;
            $timeoffset->setTimestamp($this);
        }

        return $this;
    }

    public function removeTimeoffset(Timeoffset $timeoffset): self
    {
        if ($this->timeoffset->removeElement($timeoffset)) {
            // set the owning side to null (unless already changed)
            if ($timeoffset->getTimestamp() === $this) {
                $timeoffset->setTimestamp(null);
            }
        }

        return $this;
    }
}
