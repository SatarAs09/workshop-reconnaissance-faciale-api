<?php

namespace App\Entity;

use App\Repository\TimeoffsetRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimeoffsetRepository::class)
 */
class Timeoffset
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Timestamp::class, inversedBy="timeoffset")
     */
    private ?Timestamp $timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?Timestamp
    {
        return $this->timestamp;
    }

    public function setTimestamp(?Timestamp $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
