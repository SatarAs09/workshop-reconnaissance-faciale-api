<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ImageRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity=Timestamp::class, cascade={"persist", "remove"})
     */
    private ?Timestamp $timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp() : Timestamp
    {
        return $this->timestamp;
    }

    public function setTimestamp(Timestamp $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
