<?php

namespace App\Entity;

use App\Repository\NormalisedBoundingBoxRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NormalisedBoundingBoxRepository::class)
 */
class NormalisedBoundingBox
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $top;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $left_;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $bottom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $right;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTop(): ?float
    {
        return $this->top;
    }

    public function setTop(?float $top): self
    {
        $this->top = $top;

        return $this;
    }

    public function getLeft(): ?float
    {
        return $this->left_;
    }

    public function setLeft(?float $left_): self
    {
        $this->left_ = $left_;

        return $this;
    }

    public function getBottom(): ?float
    {
        return $this->bottom;
    }

    public function setBottom(?float $bottom): self
    {
        $this->bottom = $bottom;

        return $this;
    }

    public function getRight(): ?float
    {
        return $this->right;
    }

    public function setRight(?float $right): self
    {
        $this->right = $right;

        return $this;
    }
}
