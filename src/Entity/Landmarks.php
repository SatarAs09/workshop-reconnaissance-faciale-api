<?php

namespace App\Entity;

use App\Repository\LandmarksRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;

/**
 * @ORM\Entity(repositoryClass=LandmarksRepository::class)
 */
class Landmarks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Timestamp::class, inversedBy="landmarks")
     */
    private ?Collection $timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): Collection
    {
        return $this->timestamp;
    }

    public function setTimestamp(?Timestamp $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
