<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Google\Cloud\Vision\V1\Feature\Type;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\Likelihood;
// Imports the Cloud Storage client library.
use Google\Cloud\Storage\StorageClient;


/**
 * Authenticate to a cloud client library using a service account implicitly.
 *
 * @param string $projectId The Google project ID.
 */
function auth_cloud_implicit($projectId)
{
    $config = [
        'projectId' => $projectId,
    ];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

    # Make an authenticated API request (listing storage buckets)
    foreach ($storage->buckets() as $bucket) {
        printf('Bucket: %s' . PHP_EOL, $bucket->name());
    }
}

class FaceDetectionController extends AbstractController
{
    /**
     * @Route("/face/detection", name="face_detection")
     */
    public function index(): Response
    {
        $client = new ImageAnnotatorClient();

        // Annotate an image, detecting faces.
        $annotation = $client->annotateImage(
            fopen('/data/photos/family_photo.jpg', 'r'),
            [Type::FACE_DETECTION]
        );
        return '';
        /*return $this->render('face_detection/index.html.twig', [
            'controller_name' => 'FaceDetectionController',
        ]);*/
    }
}
